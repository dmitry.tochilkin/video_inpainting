# Temporally-aware video inpainting using convolutional neural networks.

This repository contains a model for removal of objects from videos given a
mask for each frame. It extends recent image synthesis techniques to generate
spatio-temporally coherent and sharp predictions inside missing video regions.
It takes as an input a sequence of frames with missing areas concatenated with
masks and outputs frames with the areas being inpainted.

The model is trained with L1 reconstruction, preceptual, style and TV losses.
Additionally, a PatchGAN discriminator network is trained in order to penalize 
incoherent in space or time predictions. It removes flickering and helps
generator to learn simple motion propagation. Both generator and discriminator
networks incorporate (2+1)D convolutions as a basic building block in order to
being able to reliably operate in video domain but not significantly increasing
model size as in case with 3D convolutions.

Model is trained using video frames taken from Caltech Pedestrian Detection
dataset. Dense masks are extracted from DAVIS dense video segmentation dataset.

## Results
![](results/results_thesis_gif.gif)