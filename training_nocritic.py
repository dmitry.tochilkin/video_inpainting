import numpy as np
import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.init as init
import torch.optim as optim
from torch.utils.data import DataLoader
import importlib
from datetime import date

from model import building_blocks
from model import generator
from utils import logger
from utils import mask_generator
from utils import tv
from utils import video_dataset
from utils import utils


length = 2
in_channels = 3
train_bs = 32
temporal_k = 3
im_size = 64

device = 1

normalize=True

generator_norm = 'bn'
gated=False
non_local=False
n=64
model_3d = generator._3DCN(norm=generator_norm, gated=gated, do_non_local=non_local,
                           n=n, do_tanh=not normalize).cuda(device=device)
# model_3d = generator.G_3d().cuda(device=device)
print(f"Initialized a generator model with {utils.get_n_params(model_3d)} parameters.")

session_to_restore = '2805_nocritic_movmask_2D_unetG_he___n64'
model_3d.load_state_dict(torch.load(f'model/G_{session_to_restore}'), strict=False)
# critic_3d.load_state_dict(torch.load(f'model/D_{session_to_restore}'))

lr_3d = 3e-5
optimizer_3d = optim.Adam(model_3d.parameters(), lr=lr_3d, betas=(0.5, 0.999))
scheduler_3d = optim.lr_scheduler.ReduceLROnPlateau(
    optimizer_3d, 'min', factor=0.4, patience=150, verbose=True)

dataset = video_dataset.VideoDataset("../data/video/set00/",
                                     im_size=im_size, length=length)
data_loader = DataLoader(dataset, batch_size=train_bs, shuffle=True,
                         pin_memory=False, num_workers=3)
# mean_color=torch.Tensor([0.4014, 0.4118, 0.4001]) * 2 - 1
dataset_mean_color = torch.Tensor([0.4229, 0.4341, 0.4205])
if not normalize:
    dataset_mean_color = dataset_mean_color*2 - 1
dataset_std_color = torch.Tensor([0.2590, 0.2613, 0.2684])
expanded_mean_color = dataset_mean_color.view(1,3,1,1,1).expand(
    train_bs,3,length,im_size,im_size).to(device)
dataset_normalizer = utils.Normalize(dataset_mean_color, dataset_std_color, device)
dataset_unnormalizer = utils.UnNormalize(dataset_mean_color, dataset_std_color, device)

mask_gen = mask_generator.MovingMaskGenerator(length=length, mask_size=im_size)

tv_loss = tv.TVLoss()
tv_weight = 0.5

today = date.today().strftime('%d%m')
session = "{0}_nocritic_movmask_2D_unetG_he_{1}_{2}_n{3}".format(
    today, 'gated' if gated else '', 'nonlocal' if non_local else '', n)

print(session)
log = logger.Logger('./logs_video_inpainting', session)
log_step = 0


for epoch in range(0, 5):
    for i, input_batch in enumerate(data_loader):
        batch_size = input_batch.size(0)
        if normalize:
            gt = dataset_normalizer(input_batch.cuda(device=device))
        else:
            gt = input_batch.cuda(device=device)*2-1
        gt = gt.transpose(1,2)
        if batch_size != train_bs:
            continue
        # TODO: different mask for each sample in a batch.
        mask = mask_gen.generate_mask()
        expanded_mask = mask.transpose(0,1).contiguous().expand_as(gt).cuda(device=device)
        distorted = gt * (1-expanded_mask) #+ expanded_mean_color * expanded_mask
        input_3d = torch.cat((distorted, expanded_mask[:,0,:,:,:].unsqueeze(1)), dim=1)
        out_3d = model_3d(Variable(input_3d))
        pred_3d = out_3d * expanded_mask + gt * (1-expanded_mask)

        optimizer_3d.zero_grad()
        l1_err = utils.masked_l1_loss(out_3d, Variable(gt), Variable(expanded_mask), device=device)
        tv_err = tv_loss(pred_3d)
        perception_err, style_err = torch.Tensor([0]).to(device), torch.Tensor([0]).to(device)

        loss_3d = l1_err + tv_weight*tv_err
        loss_3d.backward()
        optimizer_3d.step()
        
        if i%50 == 0:
            scheduler_3d.step(loss_3d.item())
        
        if i%50 == 0:
            log_step += 1
            
            print(f"*** Step {log_step} ***")
            print("""      G_loss: {0:.3f}, L1: {1:.3f}, TV: {2:.3f}""".format(
                  loss_3d.item(), l1_err.item(), tv_err.item()))

            log.scalar_summary("lrg", scheduler_3d.optimizer.param_groups[0]['lr'], log_step)

            log.scalar_summary("G_loss", loss_3d.item(), log_step)
            log.scalar_summary("L1_loss", l1_err.item(), log_step)
            log.scalar_summary("TV_loss", tv_err.item(), log_step)

        if i%300 == 0:
            samples_to_log=10
            indices = np.arange(batch_size*length)
            np.random.shuffle(indices)
            indices = sorted(indices[:samples_to_log])
            for ind in indices:
                sample_to_log = pred_3d[ind//length,:,ind%length,...].data.cpu().numpy()
                log.image_summary('val_image_{}'.format(log_step), [sample_to_log], log_step)
                
            torch.save(model_3d.state_dict(), 'model/G_'+session)