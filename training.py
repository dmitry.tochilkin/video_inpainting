import numpy as np
import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.init as init
import torch.optim as optim
from torch.utils.data import DataLoader
import importlib
from datetime import date

from model import building_blocks
from model import discriminator
from model import generator
from utils import logger
from utils import mask_generator
from utils import tv
from utils import vgg_losses
from utils import video_dataset
from utils import utils


length = 12
in_channels = 3
train_bs = 8
temporal_k = 3
im_size = 64

device = 1

normalize=True

generator_norm = 'bn'
gated=False
non_local=True
n=64
model_3d = generator._3DCN(norm=generator_norm, gated=gated, do_non_local=non_local,
                           n=n, do_tanh=not normalize).cuda(device=device)
print(f"Initialized a generator model with {utils.get_n_params(model_3d)} parameters.")

critic_norm = 'sn'
critic_out_channels = 1
critic_n = 64
critic_gated = False
critic_nonlocal = True
critic_3d = discriminator.PatchDiscriminator3d(
    norm=critic_norm, output_channels=critic_out_channels, n=critic_n, do_non_local=critic_nonlocal
    ).cuda(device=device)
print(f"Initialized a critic model with {utils.get_n_params(critic_3d)} parameters.")

session_to_restore = '2805_movmask_unetG_he__nonlocal_n64_critic__nonlocal_n64_advw0.01'
model_3d.load_state_dict(torch.load(f'model/G_{session_to_restore}'))
critic_3d.load_state_dict(torch.load(f'model/D_{session_to_restore}'))

lr_3d = 3e-5
optimizer_3d = optim.Adam(model_3d.parameters(), lr=lr_3d, betas=(0.5, 0.999))
scheduler_3d = optim.lr_scheduler.ReduceLROnPlateau(
    optimizer_3d, 'min', factor=0.4, patience=150, verbose=True)

lr_critic = 1e-4
optimizer_critic = optim.Adam(filter(lambda p: p.requires_grad, critic_3d.parameters()), lr=lr_critic, betas=(0.5, 0.999))
scheduler_critic = optim.lr_scheduler.ReduceLROnPlateau(
    optimizer_critic, 'min', factor=0.5, patience=150, verbose=True)

dataset = video_dataset.VideoDataset("../data/video/set00/",
                                     im_size=im_size, length=length)
data_loader = DataLoader(dataset, batch_size=train_bs, shuffle=True,
                         pin_memory=False, num_workers=3)
# mean_color=torch.Tensor([0.4014, 0.4118, 0.4001]) * 2 - 1
dataset_mean_color = torch.Tensor([0.4229, 0.4341, 0.4205])
if not normalize:
    dataset_mean_color = dataset_mean_color*2 - 1
dataset_std_color = torch.Tensor([0.2590, 0.2613, 0.2684])
expanded_mean_color = dataset_mean_color.view(1,3,1,1,1).expand(
    train_bs,3,length,im_size,im_size).to(device)
dataset_normalizer = utils.Normalize(dataset_mean_color, dataset_std_color, device)
dataset_unnormalizer = utils.UnNormalize(dataset_mean_color, dataset_std_color, device)
one = torch.FloatTensor([1]).cuda(device=device)
mone = torch.FloatTensor([-1]).cuda(device=device)

mask_gen = mask_generator.MovingMaskGenerator(length=length, mask_size=im_size)

adv_weight = 0.08
gp_weight = 10
adv_loss = 'wass'
n_dis = 5

perception_weight = 0.
style_weight = 0
if perception_weight > 0 or style_weight > 0:
    vgg_loss = vgg_losses.VGGLosses(device=device)

tv_loss = tv.TVLoss()
tv_weight = 0.5

today = date.today().strftime('%d%m')
# session = "{0}_nocritic_movmask_deeperunetG_he_{1}".format(today,generator_norm)
# session = "{0}_style{9}_perception{8}_tv{7}_G_n{10}_he_{5}_3d{11}{12}PatchGAN_n{13}_out{6}_{4}_advw_{1}_gpw{2}_ndis{3}".format(
#     today,adv_weight,gp_weight,n_dis,critic_norm,generator_norm,critic_out_channels,
#     tv_weight,perception_weight,style_weight,n,
#     'gated' if critic_gated else '','nonlocal' if critic_nonlocal else '', critic_n)
session = "{0}_nopretrain_movmask_unetG_he_{1}_{2}_n{3}_critic_{4}_{5}_n{6}_advw{7}".format(
    today, 'gated' if gated else '', 'nonlocal' if non_local else '', n,
    'gated' if critic_gated else '', 'nonlocal' if critic_nonlocal else '', critic_n, adv_weight)
print(session)
log = logger.Logger('./logs_video_inpainting', session)
log_step = 0


for epoch in range(0, 5):
    for i, input_batch in enumerate(data_loader):
        batch_size = input_batch.size(0)
        if normalize:
            gt = dataset_normalizer(input_batch.cuda(device=device))
        else:
            gt = input_batch.cuda(device=device)*2-1
        gt = gt.transpose(1,2)
        if batch_size != train_bs:
            continue
        # TODO: different mask for each sample in a batch.
        mask = mask_gen.generate_mask()
        expanded_mask = mask.transpose(0,1).contiguous().expand_as(gt).cuda(device=device)
        distorted = gt * (1-expanded_mask) #+ expanded_mean_color * expanded_mask
        input_3d = torch.cat((distorted, expanded_mask[:,0,:,:,:].unsqueeze(1)), dim=1)
        out_3d = model_3d(Variable(input_3d))
        pred_3d = out_3d * expanded_mask + gt * (1-expanded_mask)
        
        # TODO: experiment with updating critic on a different mask
        for p in critic_3d.parameters():
            p.requires_grad = True 
        optimizer_critic.zero_grad()
        real_data = Variable(torch.cat((gt, expanded_mask[:,0,:,:,:].unsqueeze(1)), dim=1))
        E_critic_gt = critic_3d(real_data).mean()

        fake_data = Variable(torch.cat((pred_3d.detach(), expanded_mask[:,0,:,:,:].unsqueeze(1)), dim=1))
        E_critic_pred = critic_3d(fake_data).mean()

        if adv_loss == 'wass':
            gp = utils.calc_gradient_penalty(
                critic_3d, real_data.data, fake_data.data, batch_size, device=device)
            loss_critic = -E_critic_gt + E_critic_pred + gp_weight*gp
        else:
            raise Exception('Unknown critic loss.')
        loss_critic.backward()
        optimizer_critic.step()
    
        for p in critic_3d.parameters():
            p.requires_grad = False  # to avoid computation

        if i%n_dis == 0:
            optimizer_3d.zero_grad()
            l1_err = utils.masked_l1_loss(out_3d, Variable(gt), Variable(expanded_mask), device=device)
            E_critic_pred_G = critic_3d(torch.cat(
                [pred_3d, expanded_mask[:,0,:,:,:].unsqueeze(1)], dim=1)).mean()
            tv_err = tv_loss(pred_3d)
            perception_err, style_err = torch.Tensor([0]).to(device), torch.Tensor([0]).to(device)
            if normalize:
                if perception_weight > 0 or style_weight > 0:
                    gt_2d = gt.transpose(1,2).view(batch_size*length,3,im_size,im_size)
                    pred_2d = pred_3d.transpose(1,2).contiguous().view(batch_size*length,3,im_size,im_size)
                    perception_err, style_err = vgg_loss.get_vgg_losses(
                        dataset_unnormalizer(pred_2d), dataset_unnormalizer(gt_2d),
                        do_perception=perception_weight>0, do_style=style_weight>0)
            else:
                raise Exception("not implemented")
                perception_err, style_err = vgg_loss.get_vgg_losses(
                    (pred_2d+1)/2, (gt_2d+1)/2, do_perception=True, do_style=True)
                
            loss_3d = l1_err + adv_weight*(-E_critic_pred_G) + tv_weight*tv_err +\
                perception_weight*perception_err + style_weight*style_err
            loss_3d.backward()
            optimizer_3d.step()
        
        if i%50 == 0:
            scheduler_3d.step(loss_3d.item())
            scheduler_critic.step(loss_critic.item())
        
        if i%50 == 0:
            log_step += 1
            
            print(f"*** Step {log_step} ***")
            print("""      D_loss: {0:.3f}, E_critic_gt: {1:.3f}, E_critic_pred: {2:.3f}, GP: {3:.3f}, 
      G_loss: {4:.3f}, L1: {5:.3f}, TV: {6:.3f}, Perception: {7:.3f}, Style: {8:.4f}""".format(
                  loss_critic.item(), E_critic_gt.item(), E_critic_pred.item(), gp.item(),
                  loss_3d.item(), l1_err.item(), tv_err.item(), perception_err.item(), style_err.item()))

            log.scalar_summary("lrg", scheduler_3d.optimizer.param_groups[0]['lr'], log_step)
            log.scalar_summary("lrd", optimizer_critic.param_groups[0]['lr'], log_step)
            
            log.scalar_summary("D_loss", loss_critic.item(), log_step)
            log.scalar_summary("E_critic_gt", E_critic_gt.item(), log_step)
            log.scalar_summary("E_critic_pred", E_critic_pred.item(), log_step)
            log.scalar_summary("gradient_penalty", gp.item(), log_step)
            log.scalar_summary("G_loss", loss_3d.item(), log_step)
            log.scalar_summary("L1_loss", l1_err.item(), log_step)
            log.scalar_summary("TV_loss", tv_err.item(), log_step)
            log.scalar_summary("Perception_loss", perception_err.item(), log_step)
            log.scalar_summary("Style_loss", style_err.item(), log_step)

        if i%300 == 0:
            samples_to_log=10
            indices = np.arange(batch_size*length)
            np.random.shuffle(indices)
            indices = sorted(indices[:samples_to_log])
            for ind in indices:
                sample_to_log = pred_3d[ind//length,:,ind%batch_size,...].data.cpu().numpy()
                log.image_summary('val_image_{}'.format(log_step), [sample_to_log], log_step)
                
            torch.save(critic_3d.state_dict(), 'model/D_'+session)
            torch.save(model_3d.state_dict(), 'model/G_'+session)