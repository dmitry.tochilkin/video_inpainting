import torch
import torch.nn as nn
import math
import numpy as np
import building_blocks
import non_local


class Discriminator(nn.Module):
    def __init__(self, input_channels=4, n=32, input_size=64, max_channels=512, norm='bn'):
        super(Discriminator, self).__init__()
        self.ngpu = 1
        self.main = nn.Sequential()
        
        self.main.add_module('opening_conv', building_blocks.ConvNormReLU2d(
            input_channels, n, 5, stride=2, gated=False, norm=None))
        # state size n x input_size/2 x input_size/2
        channels = n
        for i in range(int(math.log2(input_size/4)) - 1):
            self.main.add_module(f'conv_bn_relu_{i+1}', building_blocks.ConvNormReLU2d(
            min(channels, max_channels), min(channels*2, max_channels), 3,
            stride=2, gated=False, norm=norm))
            channels *= 2
        
        channels = min(channels, max_channels)
        
        self.main.add_module('final_conv', building_blocks.ConvNormReLU2d(
            channels, channels, kernel_size=3, stride=1, norm=norm))
        self.main.add_module('output_conv', nn.Conv2d(min(channels, max_channels), 1, kernel_size=4, stride=1, padding=0, bias=True))
        # self.main.add_module('sigmoid', nn.Sigmoid())

    def forward(self, input):
        if isinstance(input.data, torch.cuda.FloatTensor) and self.ngpu > 1:
            output = nn.parallel.data_parallel(self.main, input, range(self.ngpu))
        else:
            output = self.main(input)
        return output.view(-1, 1)
                
                
class PatchDiscriminator(nn.Module):
    def __init__(self, input_channels=4, n=64, levels=3, max_channels=256,
                 norm='sn', output_channels=8, gated=True):
        super(PatchDiscriminator, self).__init__()
        self.ngpu = 1
        
        self.main = nn.Sequential()
        self.main.add_module('opening_conv', building_blocks.ConvNormReLU2d(
            input_channels, n, 5, stride=2, gated=True, norm=None))
        
        self.main.add_module('non_local', non_local.NONLocalBlock2D(n, sub_sample=True))
        channels = n
        for i in range(levels):
            self.main.add_module(f'down_{i+1}', building_blocks.ConvNormReLU2d(
                min(channels,max_channels), min(channels*2,max_channels),
                kernel_size=3, stride=2, gated=False, norm=norm))
            # self.main.add_module(f'conv_{i+1}', building_blocks.ConvNormReLU2d(
            #     min(channels*2,max_channels), min(channels*2,max_channels),
            #     kernel_size=3, stride=1, gated=False, norm=norm))
            channels *= 2
        
        channels = min(channels, max_channels)
        # self.main.add_module('dilated_1', building_blocks.ConvNormReLU2d(
        #     channels, channels, 3, stride=1, dilation=2, gated=False, norm=norm))
        # self.main.add_module('finalconv_1', building_blocks.ConvNormReLU2d(
        #     channels, channels, 3, stride=1, gated=False, norm=norm))
        self.main.add_module('finalconv_2', building_blocks.ConvNormReLU2d(
            channels, channels//4, 3, stride=1, gated=False, norm=norm))
        self.main.add_module('conv_output', nn.Conv2d(channels//4, output_channels, 3, padding=1))
        # TODO: check if sigmoid is suitable here at all.
        # self.main.add_module('sigmoid', nn.Sigmoid())
        # self.main.apply(building_blocks.init_weights)
        
    def forward(self, input):
        if isinstance(input.data, torch.cuda.FloatTensor) and self.ngpu > 1:
            output = nn.parallel.data_parallel(self.main, input, range(self.ngpu))
        else:
            output = self.main(input)

        return output

                
class PatchDiscriminator3d(nn.Module):
    def __init__(self, input_channels=4, n=48, levels=3, max_channels=256,
                 norm='sn', output_channels=8, gated=True, do_non_local=True):
        super(PatchDiscriminator3d, self).__init__()
        self.main = nn.Sequential()
        self.main.add_module('opening_conv', building_blocks.SeparableConvNormReLU3d(
            input_channels, n, temp_k=3, k=5, stride=2, padding=(1,2,2), gated=gated, norm=None))
        
        # self.main.add_module('conv_1', building_blocks.SeparableConvNormReLU3d(
        #     n, n, k=3, stride=1, gated=False, norm=norm))       
        self.main.add_module('down_1', building_blocks.SeparableConvNormReLU3d(
            n, n*2, k=3, stride=2, gated=gated, norm=norm))
        # self.main.add_module('conv_2', building_blocks.SeparableConvNormReLU3d(
        #     n*2, n*2, k=3, stride=1, gated=False, norm=norm)) 
        if do_non_local:
            self.main.add_module('non_local', non_local.NONLocalBlock3D(
                n*2, sub_sample=True))
        self.main.add_module('down_2', building_blocks.SeparableConvNormReLU3d(
            n*2, n*4, k=3, stride=2, gated=gated, norm=norm))
        # self.main.add_module('conv_3', building_blocks.SeparableConvNormReLU3d(
        #     n*4, n*4, k=3, stride=1, gated=False, norm=norm))       
        self.main.add_module('down_3', building_blocks.SeparableConvNormReLU3d(
            n*4, n*4, k=3, stride=2, gated=gated, norm=norm))
        self.main.add_module('down_4', building_blocks.SeparableConvNormReLU3d(
            n*4, n*4, k=3, stride=2, gated=gated, norm=norm))
        
        self.main.add_module('finalconv_1', building_blocks.SeparableConvNormReLU3d(
            n*4, n*2, stride=1, temp_stride=2, gated=gated, norm=norm))
        self.main.add_module('finalconv_2', building_blocks.SeparableConvNormReLU3d(
            n*2, n, stride=1, temp_stride=1, gated=gated, norm=norm))
        self.main.add_module('conv_output', nn.Conv3d(n, output_channels, (4,3,3), padding=(0,1,1)))
        # self.main.add_module('conv_output_spatial', nn.Conv3d(n, n//2, (1,3,3), padding=(0,1,1)))
        # self.main.add_module('conv_output_relu', nn.LeakyReLU(0.2, True))
        # self.main.add_module('conv_output_temporal', nn.Conv3d(
        #     n//2, output_channels, (4,1,1), padding=(0,0,0)))
        # self.main.apply(building_blocks.init_weights)
        
    def forward(self, input):
        output = self.main(input)

        return output