import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.init as init

import spectral_normalization


def init_weights(m):
    if type(m) == nn.Conv3d or type(m) == nn.Conv2d:
        try:
            init.kaiming_uniform_(m.weight, a=0.2)
        except: 
            pass  
        if m.bias is not None:
            m.bias.data.fill_(0.)


def bilinear_kernel(k, inputs, outputs):
    r = k // 2
    if k%2==1:
        center=r
    else:
        center=r+0.5

    c=np.arange(1,k+1)
    kernel_1d = np.ones((1,k)) - np.abs(c-center) / r
    kernel_2d = kernel_1d.T * kernel_1d
    return torch.from_numpy(np.tile(kernel_2d, (inputs, outputs, 1, 1))) 


class ConvNormReLU3d(nn.Module):
    def __init__(self, in_channels, out_channels, temp_k=3, k=3,
                 stride=(1,1,1), dilation=(1,1,1), padding=(1,1,1),
                 residual=False, transposed=False, bn=True, lrelu=True):
        super(ConvNormReLU3d, self).__init__()
        
        self.block = nn.Sequential()
        if not transposed:
            self.block.add_module("conv", nn.Conv3d(
                in_channels, out_channels, (temp_k, k, k), stride=stride,
                dilation=dilation, padding=padding, bias=not bn))
        else:
            self.block.add_module("conv", nn.ConvTranspose3d(
                in_channels, out_channels, (temp_k, k, k), stride=stride,
                dilation=dilation, padding=padding, bias=not bn))
        
        if bn:
            self.block.add_module("bn", nn.BatchNorm3d(out_channels))
        if lrelu:
            self.block.add_module("lrelu", nn.LeakyReLU(0.2, inplace=True))
        else:
            self.block.add_module("relu", nn.ReLU(inplace=True))
        
    def forward(self, x):
        return self.block(x)
        
class _GatedConv3d(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=(1,1,1),
                 padding=(0,0,0), dilation=(1,1,1), bias=True):
        super(_GatedConv3d, self).__init__()
        self.in_channels = in_channels
        self.conv = nn.Conv3d(
            in_channels, out_channels, (1,kernel_size,kernel_size),
            stride=stride, padding=padding, dilation=dilation, bias=bias)
        self.soft_gating = nn.Conv3d(
            in_channels, out_channels, (1,kernel_size,kernel_size),
            stride=stride, padding=padding, dilation=dilation, bias=bias)
        
        # self.conv.weight.data = torch.randn_like(self.conv.weight.data) - 0.5
        # self.norm_conv = torch.nn.utils.weight_norm(self.conv)
        # self.bias = nn.Parameter(torch.zeros((out_channels, 1, 1)), requires_grad=True)
        
    def forward(self, x):
        feature = F.leaky_relu(self.conv(x), 0.2)
        gating = F.sigmoid(self.soft_gating(x))
        
        return feature * gating
        
class SeparableConvNormReLU3d(nn.Module):
    def __init__(self, in_channels, out_channels, temp_k=3, k=3,
                 stride=1, temp_stride=1, dilation=(1,1,1), padding=(1,1,1),
                 residual=False, upsample=False, gated=False, norm='bn', lrelu=True):
        super(SeparableConvNormReLU3d, self).__init__()
        
        self.residual = residual
        self.temp_k = temp_k
        self.temp_pad = padding[0]
        self.spatial_block = nn.Sequential()
        # M = (temp_k*k*k*in_channels*out_channels) // (k*k*in_channels + temp_k*out_channels) // 2
        M = out_channels
        if upsample:
            self.spatial_block.add_module(
                "spatial_upsample", nn.Upsample(scale_factor=(1,2,2), mode='trilinear'))
        
        if gated:
            # print(stride, padding, dilation)
            self.spatial_block.add_module(
                "spatial_conv", _GatedConv3d(
                    in_channels, M, k, (1,stride,stride), (0,padding[1],padding[2]),
                    (1,dilation[1],dilation[2]), bias=norm!='bn'))
        else:
            self.spatial_block.add_module("spatial_conv", nn.Conv3d(
                in_channels, M, (1,k,k), stride=(1,stride,stride),
                dilation=(1,dilation[1],dilation[2]),
                padding=(0,padding[1],padding[2]), bias=norm!='bn'))


                # self.spatial_block.add_module("spatial_conv", nn.ConvTranspose3d(
                #     in_channels, M, (1,k,k), stride=(1,stride,stride),
                #     dilation=(1,dilation[1],dilation[2]),
                #     padding=(0,padding[1],padding[2]), bias=norm!='bn'))

        if norm=='bn':
            self.spatial_block.add_module("spatial_bn", nn.BatchNorm3d(M))
        elif norm=='lrn':
            self.spatial_block.add_module("spatial_lrn", nn.LocalResponseNorm(
                5, alpha=1, beta=1/2, k=5e-5))
        elif norm=='sn':
            init_weights(self.spatial_block[-1])
            if not gated:
                self.spatial_block[-1] = spectral_normalization.SpectralNorm(self.spatial_block[-1])
            else:
                self.spatial_block[-1].conv = spectral_normalization.SpectralNorm(self.spatial_block[-1].conv)
                self.spatial_block[-1].soft_gating = spectral_normalization.SpectralNorm(
                    self.spatial_block[-1].soft_gating)
        
        if lrelu:
            self.spatial_block.add_module("spatial_lrelu", nn.LeakyReLU(0.2, inplace=True))
        else:
            self.spatial_block.add_module("spatial_relu", nn.ReLU(inplace=True))
        
        self.temporal_block = nn.Sequential()
        self.temporal_block.add_module("temporal_conv", nn.Conv3d(
            M, out_channels, (temp_k,1,1), stride=(temp_stride,1,1), padding=0,
            dilation=(dilation[0],1,1), bias=norm!='bn'))
        if norm=='bn':
            self.temporal_block.add_module("temporal_bn", nn.BatchNorm3d(out_channels))
        elif norm=='lrn':
            self.temporal_block.add_module("temporal_lrn", nn.LocalResponseNorm(
                5, alpha=1, beta=1/2, k=5e-5))
        elif norm=='sn':
            init_weights(self.temporal_block[-1])
            self.temporal_block[-1] = spectral_normalization.SpectralNorm(self.temporal_block[-1])  
            
        if lrelu:
            self.temporal_block.add_module("temporal_lrelu", nn.LeakyReLU(0.2, inplace=True))
        else:
            self.temporal_block.add_module("temporal_relu", nn.ReLU(inplace=True))
        
    def forward(self, x):
        x = self.spatial_block(x)
        if self.temp_pad != 0:
            x = F.pad(x, (0,0,0,0,self.temp_pad,self.temp_pad), mode='replicate')
        x = self.temporal_block(x)

        return x

        
class _GatedConv2d(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=(1,1),
                 padding=(0,0), dilation=(1,1), bias=True):
        super(_GatedConv2d, self).__init__()
        self.in_channels = in_channels
        self.conv = nn.Conv2d(
            in_channels, out_channels, kernel_size, stride=stride,
            padding=padding, dilation=dilation, bias=bias)
        self.soft_gating = nn.Conv2d(
            in_channels, out_channels, kernel_size, stride=stride,
            padding=padding, dilation=dilation, bias=bias)
        
        # self.conv.weight.data = torch.randn_like(self.conv.weight.data) - 0.5
        # self.norm_conv = torch.nn.utils.weight_norm(self.conv)
        # self.bias = nn.Parameter(torch.zeros((out_channels, 1, 1)), requires_grad=True)
        
    def forward(self, x):
        feature = F.leaky_relu(self.conv(x), 0.2)
        gating = F.sigmoid(self.soft_gating(x))
        
        return feature * gating


class ConvNormReLU2d(nn.Sequential):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1,
                 dilation=1, norm='bn', lrelu=True, gated=False, transposed=False):
        super(ConvNormReLU2d, self).__init__()
        padding = dilation * (kernel_size//2)
        if not transposed:
            if gated:
                # print(stride, padding, dilation)
                conv = _GatedConv2d(in_channels, out_channels, kernel_size,
                                    stride, padding, dilation, bias = norm!='bn')
            else:
                conv = nn.Conv2d(
                    in_channels=in_channels,
                    out_channels=out_channels,
                    kernel_size=kernel_size,
                    stride=stride,
                    padding=padding,
                    dilation=dilation,
                    bias = norm!='bn')
#             if lrelu:
#                 init.kaiming_normal(conv.weight, a=0.2, nonlinearity='leaky_relu')
#             else:
#                 init.kaiming_normal(conv.weight, nonlinearity='relu')
        else:
            conv = nn.ConvTranspose2d(
                in_channels, out_channels, kernel_size, stride=2, padding=1,
                dilation=1, bias=norm!='bn')
            conv.weight.data = bilinear_kernel(4, in_channels, out_channels).float()
        
        norm_layer = None
        if norm == 'bn':
            norm_layer = nn.BatchNorm2d(num_features=out_channels, eps=1e-5, momentum=0.99)
        elif norm == 'in':
            norm_layer = nn.InstanceNorm2d(num_features=out_channels, eps=1e-5, momentum=0.99)
        elif norm == 'sn':
            conv = spectral_normalization.SpectralNorm(conv)    
#         elif norm == 'ln':
#             if fs is None:
#                 raise Exception("You need to specify feature map spatial size for layer narmalization")
#             norm_layer = nn.LayerNorm((out_channels, fs, fs))

        self.add_module('conv', conv)
        if norm_layer is not None:
            self.add_module('norm', norm_layer)
        self.add_module('relu', nn.LeakyReLU(0.2, inplace=True) if lrelu else nn.ReLU(inplace=True))

    def forward(self, x):
        return super(ConvNormReLU2d, self).forward(x)