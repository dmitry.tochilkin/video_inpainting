import torch
import torch.nn as nn

import building_blocks
import non_local


class _3DCN(nn.Module):
    def __init__(self, norm='bn', gated=True, do_non_local=True, n=64, do_tanh=False):
        super(_3DCN, self).__init__()
        self.temp_k = 3
        self.do_tanh = do_tanh
        self.do_non_local = do_non_local
        self.temp_pad = self.temp_k // 2
        self.init_conv = building_blocks.SeparableConvNormReLU3d(
            4, n, self.temp_k, 7, padding=(self.temp_pad,3,3), gated=gated, norm=None)
        self.conv_down_1 = building_blocks.SeparableConvNormReLU3d(
            n, 2*n, self.temp_k, 5, stride=2, padding=(self.temp_pad,2,2), gated=gated, norm=norm)
        self.conv_1 = building_blocks.SeparableConvNormReLU3d(
            2*n, 2*n, self.temp_k, 3, padding=(self.temp_pad,1,1), gated=gated, norm=norm)
        self.conv_down_2 = building_blocks.SeparableConvNormReLU3d(
            2*n, 4*n, self.temp_k, 3, stride=2, padding=(self.temp_pad,1,1), gated=gated, norm=norm)
        self.conv_2 = building_blocks.SeparableConvNormReLU3d(
            4*n, 4*n, self.temp_k, 3, padding=(self.temp_pad,1,1), gated=gated, norm=norm)
        
        self.conv_down_3 = building_blocks.SeparableConvNormReLU3d(
            4*n, 4*n, self.temp_k, 3, stride=2, padding=(self.temp_pad,1,1), gated=gated, norm=norm)
        self.conv_3 = building_blocks.SeparableConvNormReLU3d(
            4*n, 4*n, self.temp_k, 3, padding=(self.temp_pad,1,1), gated=gated, norm=norm)
        
        self.dilated_1 = building_blocks.SeparableConvNormReLU3d(
            4*n, 4*n, self.temp_k, 3, dilation=(1,2,2),
            padding=(self.temp_pad,2,2), gated=gated, norm=norm)
        
        # self.non_local_1 = non_local.NONLocalBlock3D(128, sub_sample=True)
        
        # self.dilated_2 = building_blocks.SeparableConvNormReLU3d(
        #     128, 256, self.temp_k, 3, dilation=(1,4,4),
        #     padding=(self.temp_pad,4,4), gated=True, norm=norm)
        # self.dilated_3 = building_blocks.SeparableConvNormReLU3d(
        #     256, 256, self.temp_k, 3, dilation=(1,8,8),
        #     padding=(self.temp_pad,8,8), gated=True, norm=norm)
        self.conv_4 = building_blocks.SeparableConvNormReLU3d(
            4*n, 4*n, self.temp_k, 3, padding=(self.temp_pad,1,1), gated=gated, norm=norm)
        # self.conv_5 = building_blocks.SeparableConvNormReLU3d(
        #     8*n, 4*n, self.temp_k, 3, padding=(self.temp_pad,1,1), gated=gated, norm=norm)
        self.conv_5 = building_blocks.SeparableConvNormReLU3d(
            4*n, 4*n, self.temp_k, 3, padding=(self.temp_pad,1,1), gated=gated, norm=norm)
        
        # self.non_local_1 = non_local.NONLocalBlock3D(4*n, sub_sample=True)
        
        self.conv_up_1 = building_blocks.SeparableConvNormReLU3d(
            4*n, 4*n, self.temp_k, 3, padding=(self.temp_pad,1,1),
            gated=gated, norm=norm, upsample=True)
        # self.conv_6 = building_blocks.SeparableConvNormReLU3d(
        #     6*n, 4*n, self.temp_k, 3, padding=(self.temp_pad,1,1), gated=False, norm=norm)
        self.conv_6 = building_blocks.SeparableConvNormReLU3d(
            4*n, 4*n, self.temp_k, 3, padding=(self.temp_pad,1,1), gated=False, norm=norm)
        if self.do_non_local:
            self.non_local_2 = non_local.NONLocalBlock3D(4*n)
        self.conv_up_2 = building_blocks.SeparableConvNormReLU3d(
            4*n, 2*n, self.temp_k, 3, padding=(self.temp_pad,1,1),
            gated=gated, norm=norm, upsample=True)       
        # self.conv_7 = building_blocks.SeparableConvNormReLU3d(
        #     4*n, 4*n, self.temp_k, 3, padding=(self.temp_pad,1,1), gated=False, norm=norm)
        self.conv_7 = building_blocks.SeparableConvNormReLU3d(
            2*n, 2*n, self.temp_k, 3, padding=(self.temp_pad,1,1), gated=False, norm=norm)
        
        # self.non_local_2 = non_local.NONLocalBlock3D(64)
        self.conv_up_3 = building_blocks.SeparableConvNormReLU3d(
            2*n, 2*n, self.temp_k, 3, padding=(self.temp_pad,1,1),
            gated=gated, norm=norm, upsample=True)        
        self.conv_8 = building_blocks.SeparableConvNormReLU3d(
            2*n, n, self.temp_k, 3, padding=(self.temp_pad,1,1), gated=False, norm=norm)
        # self.conv_9 = building_blocks.SeparableConvNormReLU3d(
        #     2*n, n, self.temp_k, 3, padding=(self.temp_pad,1,1), gated=False, norm=norm)
        self.final_conv = nn.Conv3d(
            n, 3, (self.temp_k,3,3), padding=(self.temp_pad,1,1))
        if do_tanh:
            self.tanh = nn.Tanh()
            
        self.apply(building_blocks.init_weights)
    
    def forward(self, x):
        x = self.init_conv(x)
        x = self.conv_down_1(x)
        x1 = self.conv_1(x)
        x = self.conv_down_2(x1)
        x2 = self.conv_2(x)
        
        x = self.conv_down_3(x2)
        x3 = self.conv_3(x)        
        
        x = self.dilated_1(x3)        
        # x = self.dilated_2(x)
        # x = self.dilated_3(x)
        x = self.conv_4(x)
        # x = self.conv_5(torch.cat([x, x3], dim=1))
        x = self.conv_5(x + x3)
        
        x = self.conv_up_1(x)
        # x = self.conv_6(torch.cat([x, x2], dim=1))
        x = self.conv_6(x + x2)
        if self.do_non_local:
            x = self.non_local_2(x)
        x = self.conv_up_2(x)
        # x = self.conv_7(torch.cat([x, x1], dim=1))
        x = self.conv_7(x + x1)
        
        x = self.conv_up_3(x)
        x = self.conv_8(x)
        
        # x = self.conv_9(x)
        x = self.final_conv(x)
        if self.do_tanh:
            x = self.tanh(x)
        return x


class G_3d(nn.Module):
    def __init__(self, norm='bn', gated=True):
        super(G_3d, self).__init__()
        self.temp_k = 3
        self.init_conv = building_blocks.ConvNormReLU3d(4, 16, 5, 5, padding=(2,2,2), bn=False)
        self.conv_down_1 = building_blocks.ConvNormReLU3d(
            16, 32, self.temp_k, 3, stride=(1,2,2))
        self.conv_1 = building_blocks.ConvNormReLU3d(32, 64, self.temp_k, 3)
        self.conv_down_2 = building_blocks.ConvNormReLU3d(
            64, 128, self.temp_k, 3, stride=(1,2,2))
        self.dilated_1 = building_blocks.ConvNormReLU3d(
            128, 256, self.temp_k, 3, dilation=(1,2,2), padding=(1,2,2))
        self.dilated_2 = building_blocks.ConvNormReLU3d(
            256, 256, self.temp_k, 3, dilation=(1,4,4), padding=(1,4,4))
        self.dilated_3 = building_blocks.ConvNormReLU3d(
            256, 256, self.temp_k, 3, dilation=(1,8,8), padding=(1,8,8))
        self.conv_2 = building_blocks.ConvNormReLU3d(256, 128, self.temp_k, 3)
        self.conv_up_1 = building_blocks.ConvNormReLU3d(
            256, 64, self.temp_k, 4, stride=(1,2,2), transposed=True)
        self.conv_3 = building_blocks.ConvNormReLU3d(128, 32, self.temp_k, 3)
        self.conv_up_2 = building_blocks.ConvNormReLU3d(
            64, 16, self.temp_k, 4, stride=(1,2,2), transposed=True)
        self.final_conv = nn.Conv3d(16, 3, (self.temp_k,3,3), padding=(1,1,1))
        
        self.apply(building_blocks.init_weights)
    
    def forward(self, x):
        x = self.init_conv(x)
        x1 = self.conv_down_1(x)
        x2 = self.conv_1(x1)
        x3 = self.conv_down_2(x2)
        x = self.dilated_1(x3)
        x = self.dilated_2(x)
        x = self.dilated_3(x)
        x = self.conv_2(x)
        x = self.conv_up_1(torch.cat([x, x3], dim=1))
        x = self.conv_3(torch.cat([x, x2], dim=1))
        x = self.conv_up_2(torch.cat([x, x1], dim=1))
        x = self.final_conv(x)
        return x