import torch
import torch.nn as nn
from torchvision import transforms, models

import utils


class VGGLosses(nn.Module):
    def __init__(self, layers=['pool1', 'pool2', 'pool3'], device=0):
        super(VGGLosses, self).__init__()
        self.layers = layers
        self.device = device
        self._init_pretrained_model()
        self.l1_loss = nn.L1Loss(size_average=True).cuda(device=device)
        
        vgg_mean = torch.tensor([0.485, 0.456, 0.406])
        vgg_std = torch.tensor([0.229, 0.224, 0.225])
        self.vgg_normalizer = utils.Normalize(vgg_mean, vgg_std, device, inplace=False)
        
    def _init_pretrained_model(self):    
        vgg = models.vgg19(pretrained=True).features.eval()
        self.model = nn.Sequential()
        i = 1
        j = 1
        for layer in vgg.children():
            if isinstance(layer, nn.Conv2d):
                name = f'conv{j}_{i}'.format(i)
                i+=1
            elif isinstance(layer, nn.ReLU):
                name = f'relu{j}_{i-1}'
                layer = nn.ReLU(inplace=False)
            elif isinstance(layer, nn.MaxPool2d):
                name = f'pool{j}'
                j += 1
                i = 1
            elif isinstance(layer, nn.BatchNorm2d):
                name = f'bn{j}_{i-1}'
            else:
                raise RuntimeError('Unrecognized layer: {}'.format(layer.__class__.__name__))
            self.model.add_module(name, layer)
            if name==self.layers[-1]:
                self.model = self.model.to(self.device)
                break
                
    def _get_features(self, input_batch):
        features = {}
        x = self.vgg_normalizer(input_batch.to(self.device))
#         x = input_batch.to(device)
        for name, layer in self.model._modules.items():
            x = layer(x)
            if name in self.layers:
                features[name] = x

        return features
    
    def _gram_matrix(self, tensor):
        b, d, h, w = tensor.size()
        tensor = tensor.view(b, d, h * w)
        gram = torch.matmul(tensor, tensor.permute(0,2,1))

        return gram.div_(b*d*h*w)
    
    def get_vgg_losses(self, gen, gt, do_perception=True, do_style=True):
        gen_features = self._get_features(gen)
        gt_features = self._get_features(gt)
        
        perception_loss = torch.Tensor([0]).to(self.device)
        if do_perception:
            for layer in self.layers:
                perception_loss += self.l1_loss(
                    gen_features[layer], gt_features[layer].detach())
        
        style_loss = torch.Tensor([0]).to(self.device)        
        if do_style:
            for layer in self.layers:
                style_loss += self.l1_loss(self._gram_matrix(gen_features[layer]),
                                           self._gram_matrix(gt_features[layer].detach()))
                
        return perception_loss, style_loss