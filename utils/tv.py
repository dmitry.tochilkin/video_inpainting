import torch
import torch.nn as nn
from torch.autograd import Variable

    
class TVLoss(nn.Module):
    def __init__(self):
        super(TVLoss,self).__init__()

    def forward(self,x):
        tv_col = (x[...,1:,:] - x[...,:-1,:]).abs().mean()
        tv_row = (x[...,1:] - x[...,:-1]).abs().mean()
        # print(tv_col, tv_row)
        return tv_col + tv_row