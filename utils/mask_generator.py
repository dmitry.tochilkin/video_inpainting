import cv2
import os
import numpy as np
import torch
from torchvision import transforms
from PIL import Image


class MaskGenerator():
    def __init__(self, img_size=128, max_strokes=5, min_width=15,
                 max_width=35, area_threshold=0.33):
        self.img_size = img_size
        self.max_strokes = max_strokes
        self.min_width = min_width
        self.max_width = max_width
        self.area_threshold = area_threshold
        
    def generate_mask(self):
        img = np.zeros((self.img_size, self.img_size))
        start_x=np.random.randint(0, self.img_size)
        start_y=np.random.randint(0, self.img_size)
        n_strokes=np.random.randint(1, self.max_strokes)
        for i in range(n_strokes):
            if img.sum() > self.img_size**2 * self.area_threshold:
                break
            end_x=np.random.randint(0, self.img_size)
            end_y=np.random.randint(0, self.img_size)
            width = np.random.randint(self.min_width, self.max_width)
            cv2.line(img,(start_x,start_y),(end_x,end_y),(1),width)
            start_x,start_y=end_x,end_y
        return torch.Tensor(img)
    
    def generate_mask_batch(self, batch_size):
        mask_list = [self.generate_mask() for i in range(batch_size)]
        return torch.stack(mask_list)
    

class MovingMaskGenerator():
    def __init__(self, length, mask_size,
                 root_dir="../data/video/masks/Annotations_semantics/480p/",):
        self.root_dir = root_dir
        self.length = length
        self.mask_size = mask_size
        self.valid_masks = []
        for d in os.listdir(root_dir):
            if len(os.listdir(os.path.join(root_dir, d))) >= length:
                self.valid_masks.append(d)
        self.to_tensor = transforms.ToTensor()

    def generate_mask(self):
        selected_mask = self.valid_masks[int(torch.randint(len(self.valid_masks),[1]))]
        num_frames = len(os.listdir(os.path.join(self.root_dir, selected_mask)))
        images = []
        first = int(torch.randint(num_frames - self.length, [1]))
        for ind in range(first, first+self.length):
            mask_image = self.to_tensor(Image.open(os.path.join(
                self.root_dir, selected_mask, "{:05d}.png".format(ind))))[0]
            images.append(mask_image)
        images = torch.stack(images)
        images[images>0] = 1
        
        masks = self._transform_masks(images)
        if masks.sum() < masks.nelement() / 30 or masks.sum() > masks.nelement() / 4:
            return self.generate_mask()
        else:
            return masks
        
    def _transform_masks(self, masks):       
        h = masks.size()[-2]
        w = masks.size()[-1]
        l = int(torch.randint(w - h, [1]))
        r = int(torch.randint(low=l+h, high=w, size=[1]))
        new_masks = torch.zeros((self.length,r-l,r-l))
        t = int(torch.randint(r-l-h+1, [1]))
        new_masks[:,t:h+t,:] = masks[:,:,l:r]
        masks = []
        for mask in new_masks:
            resized_mask = self.to_tensor(transforms.Resize(
                (self.mask_size,self.mask_size))(transforms.ToPILImage()(mask.unsqueeze(0)))).round()
            masks.append(resized_mask)
        return torch.stack(masks)
        