import sys
import os.path
import shutil

def delete_experiment(session):
    if os.path.exists("logs_video_inpainting/"+session):
        shutil.rmtree("logs_video_inpainting/"+session, ignore_errors=True)
    # if os.path.exists(f"model/unet_in/G_{session}"):
    #     os.remove(f"model/unet_in/G_{session}")
    # if os.path.exists(f"model/unet_in/D_{session}"):
    #     os.remove(f"model/unet_in/D_{session}")
    # for i in range(6):
    #     if os.path.exists(f"model/unet_in/G_{session}_{i}"):
    #         os.remove(f"model/unet_in/G_{session}_{i}")
    #     if os.path.exists(f"model/unet_in/D_{session}_{i}"):
    #         os.remove(f"model/unet_in/D_{session}_{i}")
            
delete_experiment(sys.argv[0])