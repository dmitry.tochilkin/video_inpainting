import torch


class Normalize(object):
    def __init__(self, mean, std, device=None, inplace=True):
        self.mean = torch.Tensor(mean)
        self.std = torch.Tensor(std)
        self.inplace = inplace
        if device is not None:
            self.mean = self.mean.to(device)
            self.std = self.std.to(device)

    def __call__(self, tensor):
        if self.inplace:
            return tensor.sub_(self.mean[...,:,None,None]).div_(self.std[...,:,None,None])
        else:
            return tensor.sub(self.mean[...,:,None,None]).div(self.std[...,:,None,None])
    
class UnNormalize(object):
    def __init__(self, mean, std, device=None, inplace=True):
        self.mean = torch.Tensor(mean)
        self.std = torch.Tensor(std)
        self.inplace = inplace
        if device is not None:
            self.mean = self.mean.to(device)
            self.std = self.std.to(device)

    def __call__(self, tensor):
        if self.inplace:
            return tensor.mul_(self.std[...,:,None,None]).add_(self.mean[...,:,None,None])
        else:
            return tensor.mul(self.std[...,:,None,None]).add(self.mean[...,:,None,None])