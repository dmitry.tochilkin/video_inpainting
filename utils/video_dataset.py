import torch
from torch.utils.data import Dataset, DataLoader
import torchvision.transforms.functional as TF
from torchvision import transforms
import numpy as np
from PIL import Image
import os
import matplotlib.pyplot as plt


class VideoDataset(Dataset):
    def __init__(self, root_dir, im_size=128, length=32, max_interval=8):
        self.sample_length = length
        self.im_size = im_size
        self.root_dir = root_dir
        self.max_interval = max_interval

        video_dirs = [name for name in os.listdir(root_dir)
            if os.path.isdir(os.path.join(root_dir, name))]
        video_lengths = [len(os.listdir(os.path.join(
            root_dir,video_dir))) for video_dir in video_dirs]
        self.video_dirs = dict(zip(video_dirs, video_lengths))
        
        self.to_tensor = transforms.ToTensor()
        self.resize = transforms.Resize((self.im_size, self.im_size))
        
    def _get_dir_and_index(self, idx):
        cum_sum = 0
        for directory, length in self.video_dirs.items():
            new_cum_sum = cum_sum + length - self.sample_length + 1
            if new_cum_sum > idx:
                return directory, idx - cum_sum
            else:
                cum_sum = new_cum_sum
        return None, None
    
    def _transform(self, images):
        width, height = images[0].size
        max_scale = self.im_size / min(width, height)
        rrc = transforms.RandomResizedCrop((self.im_size, self.im_size),
                                           scale=(max_scale, 1), ratio=(1,1))       
        do_flip = np.random.rand() > 0.5
        i, j, h, w = rrc.get_params(images[0], scale=(max_scale, 1), ratio=(1,1))
        for frame_idx in range(len(images)):
            images[frame_idx] = TF.crop(images[frame_idx], i, j, h, w)
            images[frame_idx] = self.resize(images[frame_idx])
            if do_flip:
                images[frame_idx] = TF.hflip(images[frame_idx])
            images[frame_idx] = self.to_tensor(images[frame_idx])

        return torch.stack(images)
            
    def __len__(self):
        return sum(self.video_dirs.values()) - len(self.video_dirs)*(self.sample_length-1)
        
    def __getitem__(self, idx):
        directory, index = self._get_dir_and_index(idx)
        if directory is None:
            raise Exception("No samples found.")
        images = []
        
        max_interval = min(self.max_interval, ((self.video_dirs[directory] - index - 1)
                                               // (self.sample_length - 1)))
        interval = np.random.randint(1, max_interval+1)
        
        directory = os.path.join(self.root_dir, directory)
        for i in range(self.sample_length):
            img = Image.open(os.path.join(directory, "{}.jpg".format(index + i * interval)))
            images.append(img)
        # images = self._transform(images) * 2 - 1
        # images = self.normalize(self._transform(images))
        images = self._transform(images)
        
        return images